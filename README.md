# Hola mundo!
---

_git commit -m "First Commit"_, Si esta línea te da un poco de gustete, seguramente en este blog encuentres información que te será útil. Si no tienes ni idea de lo que es, pero has llegado hasta aquí, entonces... **¡Hola Mamá!**

Este es un blog donde un par de amigos van publicando sus proyectos de forma libre y gratuita con un par de objetivos:
1. Ayudar a gente que, como nosotros, también desarrolla proyectos interesantes o inventa cacharros curiosos, y puede que necesite alguna pista durante su aventura creativa.
2. ¡Ayudarnos a nosotros mismos! Somos _como los catalanes, que hacemos cosas_ (Si has entendido esta referencia seguramente estés en la siguiente ronda de vacunación de Coronavirus), así que la mayoría de nuestros proyectos quedan perdidos en un mar de líneas de código, diseños 3D y esquemas de los que ya ni nos acordamos.

_- "Ah! Así se resuelve este bug"_
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_-Tú cuando te encuentras tu propia respuesta en un foro-_

En _First Commit_ vas a encontrar información sobre sistemas y proyectos que podrás replicar en su totalidad, pero es muy posible que también encuentres ideas nuevas sobre las que trabajar y, con un poco de suerte, alguna respuesta a ese problema que te trae por mal camino.
Aunque no tenemos una temática concreta, solemos escribir de temas relacionados con la informática, domótica, electrónica, automatización y... bueno, cacharros en general... el caso es que es poco probable que encuentres recetas para hacer cupcakes, a menos que Joseba se monte esa impresora 3D de chocolate que todo el mundo quiere.

En cualquier caso, si quieres que escribamos sobre algo en concreto, ¡aceptamos sugerencias!

https://firstcommit.dev/
