---
title: EVENT LOGGER
author: Ángel Hernández
date: 2021-03-12 20:30:00
banner:
  url: /images/relojes_inteligentes/ez430-chronos.jpg
  width: 1280
  height: 720
categories:
  - colaboraciones
tags:
  - diy
  - diwo
  - Maldita.es
  - Charla
---
¿Te suena la imagen de arriba? _Mis dieses_
Lo más normal es que no te suene de nada puesto que es un cacharro de hace más de 10 años y que no tuvo todo el impacto que podría haber tenido. 
Arduino era un jovenzuelo de 5 años cuando Texas Instruments sacó este _"reloj programable"_ con varios sensores y una pantalla LCD muy básica... básica incluso para la época, pero era un microcontrolador con pantalla que podías llevar en la muñeca... *¡por sólo 25$!... ¡¡¡en el 2010!!!*
Para mi, y para muchos frikis de la época, esta fue nuestra introducción a los _wearables_ y yo lo aproveché bastante. Aquí tenéis a un Ángel con muchas menos canas controlando un coche teledirigido con este reloj y una _Skypic_ (pero eso es una historia para otro día).
<iframe width="560" height="315" src="https://www.youtube.com/embed/DfffM5-XYOs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

¿Y por qué os cuento todo esto? Pues porque hace un par de meses colaboré en un artículo sobre _relojes inteligentes_ con [Maldita.es](https://maldita.es/), un medio de comunicación sin ánimo de lucro que me gusta leer por su neutralidad y por el buen contraste de noticias e información que hacen siempre, así que siempre es un placer poder colaborar con ellos.
En este caso, he echado una mano en un artículo titulado _[Relojes y pulseras inteligentes: hasta qué punto son fiables los datos que recogen de nuestro cuerpo](https://maldita.es/malditatecnologia/20210108/relojes-pulseras-inteligentes-fiables-datos-cuerpo/)_. 
Os animo a que le echéis un ojo si tenéis curiosidad por entender como funcionan algunos de los sensores que llevan vuestros relojes.

---
Autor: Ángel H. aka __*Mifulapirus*__   

*Si quieres recibir notificaciones cuando haya nuevas publicaciones, [suscríbete  aquí](/Subscribe/)*