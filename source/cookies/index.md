---
title: Política de cookies
date: 2021-02-10 10:00:00
---
La presente política de cookies tiene por finalidad informarle de manera clara y precisa sobre las cookies que se utilizan en la página web _firstcommit.dev_.

## ¿Qué son las cookies?

Una cookie es un pequeño fragmento de texto que los sitios web que visita envían al navegador y que permite que el sitio web recuerde información sobre su visita, como su idioma preferido y otras opciones, con el fin de facilitar su próxima visita y hacer que el sitio le resulte más útil. Las cookies desempeñan un papel muy importante y contribuyen a tener una mejor experiencia de navegación para el usuario.

## Tipos de cookies

Según quién sea la entidad que gestione el dominio desde dónde se envían las cookies y se traten los datos que se obtengan, se pueden distinguir dos tipos: cookies propias y cookies de terceros.

Existe también una segunda clasificación según el plazo de tiempo que permanecen almacenadas en el navegador del cliente, pudiendo tratarse de cookies de sesión o cookies persistentes.

Por último, existe otra clasificación con cinco tipos de cookies según la finalidad para la que se traten los datos obtenidos: cookies técnicas, cookies de personalización, cookies de análisis, cookies publicitarias y cookies de publicidad comportamental.

Para más información a este respecto puede consultar la Guía sobre el uso de las cookies de la Agencia Española de Protección de Datos.

## Cookies utilizadas en esta web

A continuación se identifican las cookies que están siendo utilizadas en este portal así como su tipología y función:

La página web del _firstcommit.dev_ utiliza __Matomo__, un servicio de analítica web, que permite la medición y análisis de la navegación en las páginas web. En su navegador podrá observar cookies de este servicio. Según la tipología anterior se trata de cookies propias, de sesión y de análisis.

A través de la analítica web se obtiene información relativa al número de usuarios que acceden a la web, el número de páginas vistas, la frecuencia y repetición de las visitas, su duración, el navegador utilizado, el operador que presta el servicio, el idioma, el terminal que utiliza y la ciudad a la que está asignada su dirección IP. Información que posibilita un mejor y más apropiado servicio por parte de este portal.

## Aceptación de la política de cookies

Pulsando el botón Entendido se asume que usted acepta el uso de cookies.

## Cómo modificar la configuración de las cookies

Usted puede restringir, bloquear o borrar las cookies de _firstcommit.dev_ o cualquier otra página web utilizando su navegador. En cada navegador la operativa es diferente, la función de _Ayuda_ le mostrará cómo hacerlo.
