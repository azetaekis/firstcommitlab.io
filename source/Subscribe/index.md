---
title: Subscribe
date: 2021-02-10 10:00:00
---

Si esperabas que la suscripción a este blog iba a ser cosa de darnos tu correo electrónico y olvidarte, es que no has entendido nada de [quienes somos](/about).

El objetivo de este blog es animarte a hacer cosas, por lo que tendrás que currartelo un poco si quieres recibir notificaciones de nuestros nuevos posts.

# Para vagos

Si eres de esas personas a las que les gusta estar en el sofa con el gato encima y no quieres líos, __hemos creado un canal de telegram__ al que puedes unirte de forma gratuita y donde llegarán, a traves de IFTTT, notificaciones por cada nuevo post que subamos al blog. Solo tienes que hacer click en el siguiente botón y unirte al canal:

<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="https://tttttt.me/firstcommit" target="_blank"><svg style="width:30px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Ahora si, de verdad, Suscríbete</a>

![cat](/images/subscribe/subscribe-10.jpg)

Sí, sabemos que no todo el mundo es como nosotros, pero teníamos que intentarlo.
:)


---
# Método DIY

Si no quieres ser parte del redil y quieres crearte tu propio canal de notificaciones para estar al día de nuestras novedades, de explicamos cómo hacerlo.

## RSS feed

Un feed RSS consiste en un archivo autogenerado por una web en el que se ofrece en un lenguaje entendible por una máquina, los nuevos post o publicaciones de dicha página.

En nuestro caso, nuestro feed RSS es en formato atom y [está aquí](https://firstcommit.dev/atom.xml).

## IFTTT

![IFTTT](/images/subscribe/subscribe-8.png)

En este mundo interconectado del IoT donde las lavadoras pueden avisarnos de cuando han terminado el ciclo de limpieza, tenemos una gran problematica llamada interconectividad. Muchos de fabricantes o proveedores de servicios web utilizan sistemas cerrados o propios lo que hace que interconectarlos entre sí sea un autentico engorro. Para salvar esta escollo, existe una servicio llamado IFTTT (IF THIS THEN THAT) que nos permite, tal y como lo dice su nombre, ejecutar automatizaciones tipo *cuando esto entonces lo otro*.

Por suerte para nosotros IFTTT ya tiene integrados cientos de miles de servicios por lo que tras esta explicación serás capáz de hacer que cuando publiquemos un nuevo post te notifique por el canal que más te guste ([que se haga un café por ejemplo](https://ifttt.com/wemo_coffeemaker)).

## Notificaciones en télegram en sólo 10 pasos

Para empezar por algo más básico, vamos a hacer que IFTTT nos envíe un mensaje de telegram cuando haya una nueva publicación:

  1. En telegram, [añade a tus contactos a IFTTT](https://t.me/ifttt) e inicia una conversación con el bot.
  2. [Créate una cuenta en IFTTT](https://ifttt.com/join)
  3. Vé a Create
![create](/images/subscribe/subscribe-1.png)
  4. Haz click en *Add* en el paso *If This*
![add](/images/subscribe/subscribe-2.png)
  5. Busca el servicio RSS
![rss](/images/subscribe/subscribe-3.png)
  6. Selecciona la opción *New feed item*
![feed](/images/subscribe/subscribe-4.png)
  7. Introduce el [link de nuestro RSS atom](https://firstcommit.dev/atom.xml) y haz click en *Create trigger*
![trigger](/images/subscribe/subscribe-5.png)
  8. Ahora vamos a buscar telegram como servicio
![telegram](/images/subscribe/subscribe-6.png)
  9. Añadis vuestros datos de telegram para que se conecte a vuestro usuario y tras eso, eliges *Send Message*
![send](/images/subscribe/subscribe-7.png)
  10. Finalmente seleccionas el *target chat* que quieras, por ejemplo *private chat* para que las notificaciones te llegan a la conversaicón que acabamos de abrir con el bot de IFTTT.
![target](/images/subscribe/subscribe-9.png)
      Pro tip: Si quieres que la notificación salga en un formato legible y bonito, cambia el *message text* por esto:
![target](/images/subscribe/subscribe-10.png)
      Podrías hacer exactamente lo mismo pero sobre una conversación de grupo o un canal.
