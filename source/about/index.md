title: About
date: 2021-02-03 11:13:28
---

Hola mundo!
_git commit -m "First Commit"_, Si esta línea te da un poco de gustete, seguramente en este blog encuentres información que te será útil. Si no tienes ni idea de lo que es, pero has llegado hasta aquí, entonces... **¡Hola Mamá!**

Este es un blog donde un par de amigos van publicando sus proyectos de forma libre y gratuita con un par de objetivos:
1. Ayudar a gente que, como nosotros, también desarrolla proyectos interesantes o inventa cacharros curiosos, y puede que necesite alguna pista durante su aventura creativa.
2. ¡Ayudarnos a nosotros mismos! Somos _como los catalanes, que hacemos cosas_ (Si has entendido esta referencia seguramente estés en la siguiente ronda de vacunación de Coronavirus), así que la mayoría de nuestros proyectos quedan perdidos en un mar de líneas de código, diseños 3D y esquemas de los que ya ni nos acordamos. 

_- "Ah! Así se resuelve este bug"_ 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_-Tú cuando te encuentras tu propia respuesta en un foro-_

En _First Commit_ vas a encontrar información sobre sistemas y proyectos que podrás replicar en su totalidad, pero es muy posible que también encuentres ideas nuevas sobre las que trabajar y, con un poco de suerte, alguna respuesta a ese problema que te trae por mal camino.
Aunque no tenemos una temática concreta, solemos escribir de temas relacionados con la informática, domótica, electrónica, automatización y... bueno, cacharros en general... el caso es que es poco probable que encuentres recetas para hacer cupcakes, a menos que Joseba se monte esa impresora 3D de chocolate que todo el mundo quiere.

En cualquier caso, si quieres que escribamos sobre algo en concreto, ¡aceptamos sugerencias!

---
Nuestra web se encuentra alojada en gitlab y [este es nuestro repositorio público](https://gitlab.com/firstcommit/firstcommit.gitlab.io).

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>