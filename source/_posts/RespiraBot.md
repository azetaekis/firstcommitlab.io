---
title: RespiraBot
date: 2021-03-20 22:00:00
banner:
  url: /images/respirabot/viseras.png
  width: 1280
  height: 720
categories:
  - diy
tags:
  - diy
  - diwo
  - Telegram
  - Bot
---
Hace ya __un año__ desde que empezó el proyecto __CovidEuskadi__ del que ya os hablamos en [este post](https://firstcommit.dev/2021/02/03/Eginzaleak/) y para celebrar el aniversario, hemos querido hacer un post explicando cómo creamos el Bot de telegram con el que muchos de vosotros hablasteis (y alguno/alguna hasta se enamoró de él).

Cuando empezamos con el grupo de [Covid Gipuzkoa](https://t.me/joinchat/VoDWMLDiO5PaDYBV) las primeras recogidas se programaron a través de formularios de Google, algo sencillo de hacer pero que obligaban a los participantes a abrir el navegador para introducir manualmente los datos. Además el control en la introducción de datos en muchos de los campos de información no era muy exhaustivo por lo que nos encontramos en muchos casos con direcciones de recogida o teléfonos de contacto erróneos impidiendo a la __DYA__ poder hacer una ruta fluida y lo más rápida posible por el territorio de Gipuzkoa.

![form](/images/respirabot/form.png)
---

# ¿Para qué nos sirve un Bot en este caso?

Se nos ocurrió entonces poner a prueba las habilidades de __@mifulapirus__ con __Python__ y el desarrollo de __Bots de Telegram__, ya que esta era la herramienta que estábamos utilizando como plataforma de comunicaciones del grupo como ya explico en el vídeo al que hacemos referencia al inicio.

_RespiraBot_ nos facilitaba bastante la captura de datos para organizar la logística de recogida de material y entrega de plástico. Simplemente con iniciar una conversación con _@RespiraBot_ e ir respondiendo a sus preguntas, se generaba la información necesaria que se subía a una hoja de cálculo en Google Drive de la cual podíamos partir para coordinar la __recogida del día siguiente__ con al DYA.

![mapa](/images/respirabot/mapa.png)

Con este sencillo Bot conversacional estábamos eliminando la necesidad de que una o dos personas estuvieran dos días repasando los datos y preguntando en el grupo por aquellos a los que les faltaban datos o eran incorrectos.

---

# ¿Cómo funciona RespiraBot?

El bot [se basa en uno de los ejemplos](https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/conversationbot.py) ofrecidos por el propio equipo de Telegram.

Cuando RespiraBot arranca, espera que alguien comience una conversación con el commando **/start**, **/empezar** o simplemente diciendo __*Vamos*__. Una vez iniciada la conversación, RespiraBot va haciendo una serie de preguntas y ofreciendo respuestas controladas cuando procede. Una vez llegado al final de la conversación, todas las respuestas se guardan y se suben a la hoja de cálculo preparada por el equipo de logística. 

![form](/images/respirabot/respirabot.jpg)

En la mayoría de preguntas incluimos respuestas con botones grandes para facilitar la estandarización de las respuestas. Además integramos la posibilidad de utilizar la __geolocalización del móvil__ para dar la dirección o la de compartir tu propio __contacto de telegram__ en vez de escribir el número de teléfono. Por detrás cogíamos el __ID único de cada usuario__ y su nick(si lo tenían público) para cruzar los datos de recogidas y entregas, y así ir calculando la cantidad de bobinas de plástico que “le tocaban” a cada participante.

Aquí puedes ver el [diagrama de flujo completo](https://raw.githubusercontent.com/Mifulapirus/RespiraBot/master/workflow.svg) de la conversación de _RespiraBot_

![digrama](/images/respirabot/workflow.svg)

Y como no podía ser de otra manera, tienes todo el código en este [repo de mifulapirus en github](https://github.com/Mifulapirus/RespiraBot) para que puedas crearte tu propio RespiraBot o lo utilices como base para crear otro para un uso diferente. A nosotros se nos ocurren miles de posibilidades como por ejemplo un Bot para programar entregas de __Pizzas__ que tanto nos gustan.

![pizza](/images/respirabot/pizza.jpg)

---

# ¿Y todo esto para qué?

Tras __uno de los meses más intensos de nuestras vidas__, nuestro pequeño Bot pasó de ser un infante a adolescente efervescente y nos ayudó a gestionar más de __15.000 viseras__ en toda Gipuzkoa fabricadas por aproximadamente __300 personas__.

Personas de aquí y de allí que por su propia voluntad y de forma totalmente altruista formaron un pequeño [ejército clonador al más estilo rep rap](https://www.reprap.org/wiki/Proyecto_Clone_Wars) y que nos hacen estar muy orgullosos de la comunidad maker de este país y de esta provincia.

![resultados](/images/respirabot/resultados.png)

[Tenéis todos los datos públicos de recogidas y entregas](https://docs.google.com/spreadsheets/d/195ryYxGoKC-m08LwXLPas-Z5JhE4AdiNZGIMSZlB8M4/edit?usp=sharing) por si queréis echarles un vistazo.

__¡GRACIAS A TODOS!__

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>
