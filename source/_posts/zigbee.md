---

title: Zigbee, secure wireless mesh network
author: yayitazale
date: 2021-03-19 20:00:00
banner:
  url: /images/zigbee/banner.png
  width: 1280
  height: 720
categories:
  - selfhosted
tags:
  - selfhosted
  - home assistant
  - guia
  - domotica
  - IoT
  - zigbee
  - mqtt

---
Cuando hablamos de __redes IoT domésticas__ la mayoría de vosotros se habrá imaginado que vamos a utilizar nuestra red WiFi para ello. Estas son las __5 razones para no usar dispositivos WiFI para IoT__:

  - __Seguridad__: tu red wifi está solo a un paso de internet a través de tu firewall, cualquier atacante podría llegar a utilizar tus bombillas para [ataques DDOS](https://en.wikipedia.org/wiki/2016_Dyn_cyberattack).
  - __Saturación de la red__: la red wifi de tu casa debería de utilizarse para lo que es, únicamente para el uso de dispositivos como móviles, tablets, portátiles etc. Si añadimos 20 bombillas, después 20 sensores, después… nuestra red acabará por saturar y nuestra velocidad de navegación se verá resentida.
  - __Alcance y velocidad__: en una red domótica buscamos que la respuesta de cada sensor y actuador sea lo más rápida posible, para que nuestras automatizaciones sean instantáneas y no tengamos que esperar 3-4 segundos a que se encienda la luz al entrar en una habitación a oscuras. Además queremos que esta velocidad sea igual en la bombilla que está al lado del router así como la que tenemos en el balcón 4 paredes más allá. Una red WiFi normal en 2,4GHz realiza conexiones directas entre cada cliente y el router, haciendo que la comunicación con los equipos más alejados sea más lenta y menos robusta.
  - __Consumo__: un chip integrado WiFi consume mucha energía. Para el caso de actuadores enchufados a la red eléctrica es algo despreciable, pero si queremos añadir sensorica alimentada por baterías estos consumos se convierten en algo muy a tener en cuenta para que el ciclo de carga/descarga aumente lo máximo posible.
  - __Gestión__: el hecho de tener todos los sensores y actuadores IoT en nuestra red WiFi al lado de todos los equipos clientes como móviles, portátiles etc complica su gestión ya que todo lo tendríamos que hacer a través del router.

Con estos contras encima de la mesa, encontramos un protocolo estándar llamado [ZigBee](https://es.wikipedia.org/wiki/Zigbee) que nos elimina todos estos problemas de golpe y porrazo. 

Si quieres navegar de forma rápida por este post, este es el TOC:

1. [Tecnología](#Tecnologia)
2. [Zigbee2MQTT](#Zigbee2MQTT)
3. [Dispositivos IoT ZigBee](#Dispositivos-IoT-ZigBee)
4. [Instalación](#Instalacion)
5. [Configuración](#Configuracion)
6. [Integracion en HASS](#Integracion-en-HASS)
7. [Emparejar dispositivos](#Emparejar-dispositivos)
8. [Grupos](#Grupos)
9. [OTA](#OTA)

---
# Tecnología

ZigBee es un conjunto de [protocolos de comunicaciones inalámbricas](https://es.wikipedia.org/wiki/IEEE_802.15.4) diseñado para __redes IoT seguras__, de __baja latencia__, __bajo consumo__ y separada de las redes habituales de comunicaciones como el WiFi. Para esta separación tendremos un gateway securizado que __dividirá físicamente__ nuestra red local LAN con nuestra red IoT.

![gateway](/images/zigbee/1.png)

La topología de red es de tipo [mesh (malla)](https://es.wikipedia.org/wiki/Red_en_malla), una topología que nos permite que todos los elementos IoT que añadamos a la red se conviertan en __nodos operativos__ que transportan información. De esta forma, el Gateway de la red ZigBee puede encontrarse en una punta de nuestra casa pero los dispositivos más alejados de la misma tendrán una cobertura asegurada ya que se conectarán al nodo más cercano que encuentre, y este a su vez al siguiente.

Además incluye entre sus protocolos uno para el cálculo de [Shortest Path Bridging](https://es.wikipedia.org/wiki/IEEE_802.1aq), algo muy útil en redes mesh ya que es el mismo sistema quien decidirá de forma autónoma las rutas más cortas y eficientes para el envío de información entre el Gateway y los dispositivos, consiguiendo así latencias bajas.

![mesh](/images/zigbee/2.gif)

En una red IoT no necesitamos un ancho de banda muy grande ya que los paquetes de información son pequeños, lo que necesitamos es una latencia baja para que esos paquetes viajen de la forma más rápida posible. Esto hace que los chips ZigBee están diseñados para consumir una mínima cantidad de energía posible.

Las alternativas que se te habrán ocurrido leyendo esto seguro que son el [Bluetooth Low Energy](https://es.wikipedia.org/wiki/Bluetooth_de_baja_energ%C3%ADa), [Z-Wave](https://en.wikipedia.org/wiki/Z-Wave) o [Lora](https://es.wikipedia.org/wiki/LoRaWAN), pero este tipo de protocolos tiene algunos aspectos en su contra que los hacen menos prácticos para una red IoT doméstica:

  - __Bluetooth Low Energy o BLE__: el consumo energético sigue siendo mayor al de ZigBee. El hardware es bastante más complejo de fabricar por lo que es más caro de implementar. La capama de comunicación es más compleja. Es cierto que se pueden crear redes Mesh pero no es algo habitual.
  - __Z-Wave__: Es algo muy parecido a ZigBee. La diferencia sustancial es la banda de frecuencia que utilizan, la cual implica una menor tasa de transferencia, y además que el tipo de chip utilizado es propietario (hay que pasar por caja si quieres hacer que tu dispositivo sea Z-Wave)
  - __Lora__: es un tipo de red pensado más para redes IoT de exterior o globales, como podría ser un ejemplo, un sistema de sensorización de coches. Está concebido para crear redes amplias de bajo consumo.

Como véis estos protocolos podrían perfectamente ser los elegidos para nuestra red IoT doméstica pero a todo esto hay que añadir que [ZigBee es ya el protocolo elegido por grandes multinacionales](https://zigbeealliance.org/market-uses/smart-home/) como Philips, Samsung, Amazon, Ikea, Xiaomi, Lidl, Osram. Esto es algo importante a la hora de encontrar sensores y actuadores compatibles con nuestra red y además a buen precio.

![alliance](/images/zigbee/3.svg)

Ahora que hemos elegido ZigBee como protocolo de nuestra red, nos encontramos con que aún siendo un estándar de comunicaciones, cada fabricante nos pide que utilicemos su gateway ZigBee (por ejemplo, [xiaomi](https://amzn.to/3lygxug) o [ikea](https://www.ikea.com/es/es/p/tradfri-dispositivo-conexion-blanco-40337806/)). Esto es un auténtico engorro ya que nos interesa poder tener una __interoperabilidad__ que no nos ate a tener que adquirir los sensores o actuadores de marcas concretas según nuestras necesidades. 

Por suerte para nosotros, como hemos dicho, el protocolo ZigBee es un estándar por lo que, como es habitual, existen proyectos de __Software Libre__ que nos permiten utilizar un gateway genérico al que conectaremos cualquier tipo de dispositivo que podamos comprar.Además, como vimos en el [anterior post](https://firstcommit.dev/2021/03/14/mqtt/), nuestro objetivo es comunicarnos con estos dispositivos utilizando el protocolo MQTT… y FUUUUSIÓN:

![fusion](/images/zigbee/4.jpg)

---

# Zigbee2MQTT

Estamos de enhorabuena. Este proyecto, que es muy muy fácil de instalar, nos permite integrar en nuestro Home Asisstant cualquier dispositivo ZigBee a través de un gateway genérico. 

![arquitectura](/images/zigbee/4.png)

Vamos a ponernos un poco técnicos. Una red zigbee requiere de los distintos elementos:

  - __Coordinador__: es el gateway de la red, el que comunicará nuestra red ZigBee con la red LAN. Debe existir __solo uno por red__ y sus funciones son las de encargarse de controlar la red y el path planning que hemos comentado antes.
  - __Router__: dispositivos que funcionan como repetidores. Pueden ser equipos que únicamente hacen de repetidores o también dispositivos IoT conectados a la red eléctrica como las bombillas. Serán los __nodos activos__ de nuestra red mesh por los que viajará la información. Nunca entran en modo sleep.
  - __Dispositivo final__: sensores o actuadores con chips pasivos. Son los dispositivos que funcionan a baterías, entrarán en __modo reposo__ cuando no haya nada de qué informar o hacer reduciendo su consumo al mínimo. Son los __nodos finales__ por los que no viaja información.

![nodos](/images/zigbee/5.png)

Teniendo esto en cuenta, debemos saber que cada tipo de chip coordinador tendrá especificado de fábrica cuantos router y dispositivos finales puede gestionar. Normalmente esto además depende del firmware que lleve dicho coordinador. Actualmente el protocolo ZigBee va por la [versión 3.0](https://www.ti.com/lit/an/swra615a/swra615a.pdf?ts=1616174199575&ref_url=https%253A%252F%252Fduckduckgo.com%252F) (backwards compatible con las versiones anteriores) el cual por definición permite redes de mayor tamaño. 

Es importante antes de lanzaros a la piscina calcular cuántos dispositivos vais a añadir a la red, de forma que compraremos el coordinador que mejor se adapte a nuestras necesidades. Por los precio actuales yo os recomiendo empezar directamente por un coordinador con chip [CC2652RB](https://www.ti.com/product/CC2652RB) (con el firmware 3.X instalado) ya que nos permitirá gestionar hasta [200 dispositivos IoT](https://github.com/Koenkk/Z-Stack-firmware/tree/master/coordinator) por un precio razonable, siendo la opción de mejor rendimiento/precio. 

Existen dos opciones que sabemos que funciona bien:

  - Slaesh's CC2652RB stick ([26 €](https://slae.sh/projects/cc2652/))
    ![Slaesh](/images/zigbee/6.jpg)

  - zzh! CC2652R Multiprotocol RF Stick ([25 €](https://www.tindie.com/products/electrolama/zzh-cc2652r-multiprotocol-rf-stick/))
    ![zzh!](/images/zigbee/7.jpg)


En ambos casos no hay stock en estos momentos ya que como sabréis hay [problemas de stock de microchips a nivel mundial](https://www.newscientist.com/article/2271918-theres-a-global-shortage-of-computer-chips-whats-causing-it/). Mi recomendación (es el que yo tengo) es el __stick de slaesh’s__ con la antena tipo 2 o 4 y con el __firmware 3.0 ya instalado__ para evitarnos ese paso.

Otra alternativa es un stick Texas Instruments CC2531 ([15 € el kit con el flasheador](https://es.aliexpress.com/item/4000439492385.html?spm=a2g0o.productlist.0.0.70f41e35b3uYlt&algo_pvid=46dbc991-f47f-4f98-bb69-344b3c5d8f97&algo_expid=46dbc991-f47f-4f98-bb69-344b3c5d8f97-1&btsid=2100bdd516161550762821715ea2cb&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)), aunque tendrás que flashear tu mismo el firmware y además solo podrás agregar 40 dispositivos como máximo.

Es muy recomendable comprar también un [alargador USB](https://amzn.to/3qZZVNf) para alejar el stick y su antena del servidor ya que este puede crear interferencias de señal. En mi caso además el servidor vive dentro de un pequeño armario metálico por lo que para evitar el efecto jaula de faraday es indispensable.

---

# Dispositivos IoT ZigBee

Mientras esperas a que te llegue el stick, podemos dar un vistazo a la propia [página de dispositivos “compatibles”](https://www.zigbee2mqtt.io/information/supported_devices.html) del propio Zigbee2Mqtt. Teóricamente cualquier dispositivo ZigBee será compatible de serie, pero en esta página se listan todos los dispositivos que alguien de la comunidad ha testeado y explicado cómo proceder al emparejamiento etc. 

Como podemos ver, todos los dispositivos de las marcas que he comentado antes están listadas (Philips, Samsung, Amazon, Ikea, Xiaomi, Lidl, Osram), además de un sinfin de marcas menos conocidas. La lista es ya __prácticamente interminable__. 

![aqara](/images/zigbee/8.jpg)

Aquí ya está en tus manos seleccionar los dispositivos que se ajusten a tus necesidades, pero yo os voy a explicar la instalación actual que tengo:

  - [Bombillas Ikea](https://www.ikea.com/es/es/cat/smart-lighting-36812/): baratas y de muy diferentes tipos. En concreto, tengo las Leptiter en cocinas y baños, tiras de led TRADFRI en la cocina para iluminar las encimeras, las TRADFRI GU10 para el pasillo y armarios empotrados y las bombillas TRADFRI normales E27 de distintas potencias para las lámparas de techo etc. Son muy baratas, fáciles de comprar y la gama es muy amplia. Además si el día de mañana se estropee alguna creo que será fácil que pueda comprar una exactamente igual.
  - [Sensores de presencia Xiaomi](https://amzn.to/30Vsh0y): los sensores volumétricos de Ikea no me gustan ya que su respuesta es lenta y además el rango de visión es bajo (aparte de ser muy grandes y feos). Xiaomi tiene una gama llamada Aqara para domótica y unos sensores volumétricos pequeños que puedes disimular de forma fácil. Utilizan pilas de botón 2450 que duran aproximadamente DOS AÑOS. Además tengo pilas recargables de LIR2450 de litio que aunque duran un poco menos que las alcalinas (un año aproximadamente), las recargo en 30 minutos. Los sensores vienen con pilas alcalinas así que tienes dos años todavía para pensarlo.
  - [Sensores de humedad Xiaomi](https://amzn.to/3r3lo8f): de la misma gama Aqara que los volumétricos. Estos van con pilas de botón 2032 y duran aproximadamente año y medio.
  - [Sensores de puertas y ventanas Xiaomi](https://amzn.to/3s3i5Pn): también son Aqara, usan pilas CR1632 y llevo ya 3 años sin necesidad de cambiarlas. 
  - [Enchufes inteligentes Osram](https://amzn.to/30XTakr): los hay de ikea pero son más grandes. ni miden la potencia consumida, por lo que solo los uso para on/off
  - [Enchufes Xiaomi power plug](https://amzn.to/3r0fAMr): si quiero controlar el enchufe y medir la potencia consumida, utilizo estos
  - [Bombillas Osram Substitube T8 Advanced UO](https://www.ledvance.com/professional/products/product-stories/led-tubes-online-special/osram-substitube-t8-connected/index.jsp): los tubos fluorescentes del garaje también los tengo cambiados por unos ZigBee
  - También tengo algún [botón Aqara](https://amzn.to/3r1JiRr) para acciones manuales
  - Finalmente para los dispositivos que van directos a la red eléctrica como el extractor de humedad de los baños, los tengo a través de un [Sonoff Basic ZB R3](https://amzn.to/3s2FsZH)

Como ves aquí ya entra en juego tu imaginación, presupuesto y ganas de automatizar los diferentes elementos de tu casa.

---

# Instalación

La instalación de Zigbee2Mqtt es bastante simple. Lo primero de todo es enchufar nuestro stick ZigBee ya flasheado a uno de los __puertos USB del panel trasero__ de nuestro servidor usando el alargador de USB. En unraid vamos a la pestaña *Tools* y entramos en *System Devices* y miramos en el apartado USB Devices si aparece algo como *Cygnal Integrated Products, Inc. CP2102/CP2109 UART Bridge Controller [CP210x family]*

![decives](/images/zigbee/8.png)

![usb](/images/zigbee/9.png)

(Si no aparece, convendría reiniciar el servidor con el stick enchufado. Para ello vamos a la pestaña *main* y damos a *reboot* y esperamos. Esto en principio no debería ser necesario.)

Ahora vamos a la pestaña *Apps* y buscamos por Zigbee2Mqtt:

![app](/images/zigbee/10.png)

Nos abrirá la plantilla y aquí, nos cargará por defecto el repositorio *koenkk/zigbee2mqtt*:

![plantilla](/images/zigbee/11.png)

Si hemos instalado un __stick CC2531__ podemos seguir adelante tal y como está, pero en caso de usar un __CC2652RB__ existe un __bug en la última versión__ que hace que en algunos casos no funcione bien por lo que vamos a cambiarlo por *koenkk/zigbee2mqtt:1.17.1* siendo la versión 1.17.1 la última que sabemos que funciona bien. Esto esperemos que se arregle en el futuro y podamos volver de nuevo al repositorio “latest” para seguir al día con los parches de seguridad y mejoras.

La variable de device sirve para pasarle el stick USB al contenedor. Para poder hacerlo, necesitamos saber en qué ruta se encuentra el stick USB en nuestro host. Para ello vamos a abrir una consola de unraid haciendo click aquí:

![consola](/images/zigbee/12.png)

Escribimos el siguiente comando

> ls -l /dev/serial/by-id

![dev](/images/zigbee/13.png)

Miramos lo que pone al final y lo ponemos igual precedido por */dev/* en el campo device, en mi caso:

> /dev/ttyUSB0

![plantilla2](/images/zigbee/14.png)

Aplicamos y esperamos a que arranque. 

---

# Configuracion

Lo primero es que vayamos a *\\IP-DE-NUESTRO-UNRAID\mqtt* y creemos un juego de __usuario/contraseña__ como vimos en el [anterior post](https://firstcommit.dev/2021/03/14/mqtt/#Configuracion). Apuntamos estos datos ya que los vamos a introducir en la configuracion de Zigbee2MQTT.

Ahora vamos a la ruta *\\IP-DE-NUESTRO-UNRAID\appdata\zigbee2mqtt* y veremos una serie de archivos. Abrimos el archivo *configuration.yaml* con nuestro editor de código y vamos a configurar algunos aspectos. Vamos dejar el archivo de esta manera:

![config](/images/zigbee/15.png)

  - __homeassitant__: lo activamos para que se integre
  - __permit join__: por ahora lo dejamos en false, luego activaremos esto a demanda desde la interfaz web
  - __mqtt__: debes introducir la IP de tu servidor sin el puerto MQTT y el user/pass que acabas de crear
  - __serial__: añadimos lo mismo que el la variable device que hemos pasado al contenedor
  - Finalmente añadimos __experimental__ y __frontend__ para que nos cargue la interfaz WEB que nos facilitará muchísimo el trabajo.

Guardamos el archivo *(Si no nos deja guardarlo directamente en la ruta por tema permisos de linux, lo que debemos hacer es guardarlo en nuestro escritorio del pc, borramos el archivo original en la ruta compartida y después copiamos el archivo modificado del escritorio a la ruta. Esto es algo que nos ocurrirá con frecuencia con servicios nuevos)*.

Ahora reiniciamos el contenedor y vamos abrir la ventana de LOGS para ver que todo arranca correctamente:

![logs](/images/zigbee/16.png)

![logs2](/images/zigbee/17.png)

Una vez arrancado, en el navegador abrimos una nueva ventana y vamos a *http://IP-DE-UNRAID:8810* y veremos la interfaz web desde donde gestionaremos todos los dispositivos.

---

# Integracion en HASS

Como hemos visto Zigbee2Mqtt se encarga de transformar todas las comunicaciones MQTT que hagamos desde Home Asisstant a la red ZigBee. Si seguiste los pasos del [post anterior](https://firstcommit.dev/2021/03/14/mqtt/) ya tendrás tu Broker MQTT integrado con Home Assistant y con el modo __Autodiscovery activado__, por lo que no es necesario hacer absolutamente nada. Podemos pasar a emparejar dispositivos directamente.

---

# Emparejar dispositivos

Como hemos visto, hay infinidad de dispositivos compatibles con nuestro sistema y cada uno de ellos tendrá un sistema de emparejamiento diferente. Por tanto, lo mejor es buscar en el manual del fabricante o en la página del dispositivo en el [listado de compatibles](https://www.zigbee2mqtt.io/information/supported_devices.html) cómo podemos poner nuestro dispositivo en modo emparejamiento. 

Una vez que lo tengamos claro y antes de poner el dispositivo en ese modo, vamos a la interfaz web de Zigbee2Mqtt *http://IP-DE-UNRAID:8810* y hacemos click en *permit join*. Esto hará que nuestro coordinator se ponga a la espera de encontrar dispositivos en modo emparejamiento.

![join](/images/zigbee/18.png)

Una vez hecho esto, empezaremos a poner los diferentes dispositivos en modo emparejamiento uno a uno. Como hemos visto antes, podemos diferenciar los dispositivos por nodos activos o pasivos. Es conveniente que __empieces emparejando primero los que serán nodos activos__ (bombillas, enchufes etc) ya que los coordinadores tienen una limitación de cuando nodos pasivos pueden tener conectados directamente a ellos. La idea es crear una red mesh empezando por los nodos del centro y después encender los nodos finales.

Estas son las típicas formas de entrar en modo emparejamiento:

  - Pulsar el botón durante 10 segundos
  - Encender y apagar la bombilla 6 veces seguidas de forma rápida
  - En algunos casos en el primer encendido el dispositivo se pone en modo emparejamiento por defecto.

Una vez veamos que el dispositivo se ha emparejado nos aparecerá un mensaje en verde en la interfaz web y aparecerá en la lista. Cuando terminemos de emparejar dispositivos ya podemos volver a dejar el *join* desactivado, no vaya a ser que algún vecino compre bombillas de ikea y se emparejen a nuestro sistema :).

Si vemos en esta lista en el lado derecho tenemos un botón para renombrar el dispositivo. Esto modificará el *friendly_name* en esta interfaz como el *entity_id* en Home Assistant, algo muy útil para que identificarlo sea mucho más sencillo:

![rename](/images/zigbee/19.png)

![rename2](/images/zigbee/20.png)

Si hacemos click en su nombre podremos ver los diferentes apartados de un dispositivo, como por ejemplo los elementos que expone hacia fuera. Cada uno de estos exposes será un sensor o actuador en Hass:

![exposes](/images/zigbee/21.png)

Una vez hecho todo esto ya podemos ir a nuestro Hass, vamos a herramientas para desarroladores y aquí buscamos por nuestro *entity_id*. Si hacemos clic podremos probar a encender y apagar una luz o ver los datos de algun sensor:

![desarrolador](/images/zigbee/22.png)

![pasillo_1](/images/zigbee/23.png)

---

# Grupos

Una de las funciones más interesantes de Zigbee2Mqtt es la posibilidad de crear __grupos de actuadores__. Esto nos permite juntar las luces de una estancia para que __se enciendan y apaguen a la vez__. Si en la interfaz web de Zigbee2mqtt vamos a la pestaña *Groups* vamos a crear un grupo de las luces del pasillo:

![grupo_pasillo](/images/zigbee/24.png)

Ahora añadimos las distintas luces:

![grupo_pasillo_2](/images/zigbee/25.png)

Estos grupos __no se crean automáticamente en Hass__ por lo que ahora nos toca crearlos a mano. Vamos a la ruta comapartida de los archivos de home assisstant: *\\IP-DE-NUESTRO-UNRAID\home-assistant-core* y abrimos el archivo *configuration.yaml*

Nos posicionamos al final del todo y añadimos lo siguiente:

> light: !include lights.yaml

Guardamos y cerramos. Ahora creamos un nuevo archivo en la misma ruta llamado *lights.yaml*. De esta forma el archivo de configuración general queda más limpio y en este nuevo archivo añadiremos los grupos. Como hemos dicho vamos a añadir el grupo pasillo escribiendo lo siguiente:

> - platform: mqtt
    schema: json
    name: Pasillo
    command_topic: "zigbee2mqtt/pasillo/set"
    state_topic: "zigbee2mqtt/pasillo"
    color_temp: true
    brightness: true
    rgb: false

En este caso mis bombillas del pasillo tienen los *exposed* de **color_temp** (permite cambiar el tono de blanco) y **brightness** (cantidad de brillo) por lo que estos dos los pongo en *true*. Sin embargo, al no ser bombillas RGB esta última opción lo pongo en false.

Guardamos el archivo y volvemos a nuestro Hass. Aquí vamos a *configuración* > *controles del servidor* y hacemos click en *verificar configuración*. Si hemos hecho todo bien nos dirá que la configuración es correcta y hacemos clic en *reiniciar* para que coja los cambios.

![validar_1](/images/zigbee/26.png)

![validar_2](/images/zigbee/27.png)

Una vez reiniciado volvemos a herramientas para desarrolladores y buscamos por el grupo pasillo y vemos que podemos encender todas las luces a la vez con un solo botón.

![grupo_1](/images/zigbee/28.png)

![grupo_2](/images/zigbee/29.png)

Esta forma de crear grupos es mejor hacerla de esta forma que os he explicado ya que lo que hemos hecho es crear un __grupo virtual en el coordinador__. Esto hace que desde hass solo salga __un solo mensaje MQTT__ al coordinador para que encienda el grupo y este a su vez enviará la señal de encendido de forma __simultánea y sincronizada__ a todos los nodos calculando la ruta a la vez.

Existe la forma de crear los grupos en Hass pero el funcionamiento sería justo lo contrario: desde hass se emitirán tantos mensajes MQTT como bombillas en el grupo y el coordinador irá procesandolos de uno en uno esperando la respuesta del nodo antes de seguir con el siguiente, por lo que sí tenemos la red algo saturada podremos encontrarnos con problemas de sincronismo en el encendido de las luces a parte de saturar el procesamiento del chip ZigBee en balde. 

---
# OTA

Por último quiero mencionar que ZIgbee2Mqtt incluye un __sistema de actualización OTA__ (Over The Air) de nuestros dispositivos IOT desde la propia interfaz. Solo tienes que ir a la pestaña OTA, hacer clic en check y si encuentra actualizaciones disponibles te mostrará el dispositivo con un botón rojo.

![ota_1](/images/zigbee/30.png)

![ota_2](/images/zigbee/31.png)

Este sistema de actualización es algo que todavía está en __fase beta__ por lo que ten cuidado al usarlo, puede que alguno de tus dispositivos se rompa. Además debes tener en cuenta que, como hemos dicho, las redes ZigBee tienen un __ancho de banda limitado__ por lo que es aconsejable que solo actualices un dispositivo por vez para no saturar la red.

---

Y hasta aquí este post sobre ZigBee. En el [próximo post](https://firstcommit.dev/2021/04/05/home-automation/) veremos con más detalle la creación de pantallas __frontend__ en Hass así como __automatizaciones simples__, y también aprenderemos a aplicar __themes__ para dejar el dashboard a nuestro gusto.

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>

