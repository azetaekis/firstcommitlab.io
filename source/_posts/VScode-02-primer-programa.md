---
title: Programando un Arduino con VS Code
author: mifulapirus
date: 2021-03-31 20:30:00
banner:
  url: /images/VScode/warrior_banner_low.jpg
  width: 1280
  height: 720
categories:
  - diy
tags:
  - diy
  - diwo
  - ESP8266
  - Arduino
  - Programación
---
Bien, supongo que ya has instalado _VS Code_ y _Platform IO_, así que ahora empieza lo divertido. Vamos a programar un Arduino. De momento va a ser algo sencillo, un Arduino normalito conectado por USB igual que lo harías en el IDE de Arduino.

¡Empezamos!

Programemos un Arduino
======================
1. Abre _VS Code_ y ve a _PlatformIO_ --> _PIO Home_ --> _Open_,  en caso de que no esté ya abierto. Aprovecha para hacer que _Platform IO_ no se abra al inicio, que es un incordio. Después, selecciona _New Project_ 
<img src="/images/VScode/pio-new_project.jpg" alt="Platform IO New Project" class="sombra" />

2. Ponle un nombre, selecciona tu Microcontrolador y asegúrate de estar poniendo el proyecto en el directorio adecuado. Si no cambias el directorio, estarás usando el que _PlatformIO_ crea por defecto y, si eres una persona ordenada, es muy probable que no sea la carpeta adecuada.
<img src="/images/VScode/pio-new_project_settings.jpg" alt="Platform IO New Project" class="sombra" />

3. Verás que _PlatformIO_ ha creado unos cuantos directorios y archivos. Iremos viendo más adelante que hay en cada uno. De momento abre el archivo _main.c_ que se encuentra en la carpeta _scr_. <img src="/images/VScode/pio-file_tree.jpg" alt="pio-file_tree" class="sombra zoom"/> _main.c_ es tu archivo principal donde tienes la típica rutina de _setup_ y el _loop_ principal.

4. ¿Ves la línea 1 que dice _#include <Arduino.h>_? No la pierdas de vista. Siempre que trabajes con Arduinos, te hará falta puesto que es quien contiene los encabezados necesarios.
<img src="/images/VScode/pio-main_arduino_line.jpg" alt="pio-file_tree" class="sombra" />

5. Sustituye las funciones de setup y loop por el siguiente código incluyendo los comentarios, que aparecerán como parte del tool-tip de la función.
```Arduino
/* Configuración general */
void setup() {
  pinMode(LED_BUILTIN, OUTPUT); //Configura el LED como SALIDA
}

/* Loop principal
 * Hace que parpadee el LED cada segundo
 */
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);                    
  digitalWrite(LED_BUILTIN, LOW); 
  delay(1000);                    
}
```

 6. Conecta tu Arduino y sube el código desde `PlatformIO --> General --> Upload` o dándole al botón de la flecha <img src="/images/VScode/pio-upload-single.jpg" alt="pio-upload" class="sombra-inline" style="display: inline-block; "/> 
<img src="/images/VScode/pio-upload-menu.jpg" alt="pio-upload-menu" class="sombra"/>

Y ya está... no tiene mucho más misterio que saber dónde están las herramientas e ir acostumbrándote poco a poco.
[<img src="/images/VScode/platipus.png" alt="ya esta"/>](https://t.me/addstickers/MyNiffler)

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>