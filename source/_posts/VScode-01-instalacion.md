---
title: VS Code + Platform IO, el IDE definitivo.
author: mifulapirus
date: 2021-03-29 17:30:00
banner:
  url: /images/VScode/vscode_banner_low.jpg
  width: 1280
  height: 720
categories:
  - diy
tags:
  - diy
  - diwo
  - ESP8266
  - Arduino
  - Programación
---

Si estás leyendo esto es porque seguro que cacharreas con microcontroladores, Python, Raspberry, domótica o proyectos que te obliguen a escribir código, compilar, ejecutar y "debuguear"... si te sueles encontrar con un montón de IDEs abiertos y teniendo que cambiar de uno a otro dependiendo del lenguaje, del hardware o del proyecto, sigue leyendo. Esto te va a interesar.
¿No sería fantástico poder usar un IDE libre y gratuito que resuelva todas tus necesidades?
Hoy empezamos una serie de artículos sobre _Visual Studio Code_ para echaros una manos en la transición hacia una programación más cómoda y agil. 
Esto es lo que vas a poder hacer desde un único IDE al terminar con estos tutoriales:
- ¡Quitarte de encima el IDE de Arduino!
- Programar otros microcontroladores como ESP32 o ESP8266.
- Usar librerías desde los repositorios originales de GitHub.
- Abrir y leer Puertos Serie.
- Usar Git sin necesidad de comandos.
- Programar en Python.
- Crear webs en HTML con CSS
- Conectarte en remoto a tu Raspberry sin salir del IDE
- Acceder a servidores remotos.

Empecemos por el principio!

Instalación de VS Code y PlatformIO
===================================
Empecemos por la parte que más me ha ayudado a mi. Vamos a eliminar el IDE de Arduino, que, junto con la web de RENFE, es el peor software de este siglo.
Sólo tenemos que instalar Visual Studio, que será nuestro entorno de trabajo de aquí en adelante y _PlatformIO_, la plataforma de programación para sistemas embebidos como Arduinos o ESPs.

**0.** Si puedes (por eso es el paso 0), desinstala todas las versiones de Python que tengas. Si Solo tienes Python 3.x es posible que no tengas problemas. Si tienes más versiones y alguno de los siguientes pasos te da problemas, échale un ojo a [esta sección](#FAQ).

1. Descarga Visual Studio Code: Hay muchas versiones de _Visual Studio_, pero no te dejes liar. Olvídate de cualquiera que no sea _VS code_. Bájatelo desde aquí: https://code.visualstudio.com/
2. Sigue los pasos de instalación
<img src="/images/VScode/vscode_instalado.jpg" alt="vscode fresh install" class="sombra" />

3. Abre _Visual Studio Code_, vete al menú de extensiones y escribe _platformio_
<img src="/images/VScode/platformio-install.jpg" alt="Instalar PlatformIO" class="sombra" />

4. Selecciona Instalar y espera a que te aparezca el logo en la izquierda. 
<img src="/images/VScode/platformio-installed.jpg" alt="PlatformIO icono" class="sombra" />

5. Reinicia _VS code_ y espera a que _Platform IO_ termine de instalarse.
<img src="/images/VScode/platformio-installed_success.jpg" alt="PlatformIO finalizado" class="sombra" />

6. Ya estás listo para empezar a programar tus Arduinos, así que podemos continuar con un ejemplo.

Ahora ya estás listo para empezar a programar tus microcontroladores con _VS Code_ y _PlatformIO_. Si te ha fallado algo, échale un ojo al la siguiente sección, pero si si quieres empezar a programar algo, continúa leyendo el siguiente artículo, que saldrá en un par de días.


FAQ
====
- Platform IO no termina de instalar y da un error similar a _subprocess.CalledProcessError_
Es muy posible que esté relacionado con una instalación previa de Python. Prueba lo siguiente:

  1. Dale a la tecla de Windows y empieza a escribir _alias de ejecución de aplicaciones_
  2. Deshabilita Python y Python3
  3. Reinicia _VS Code_

Si esto no te funciona, echale un ojo a [estas posibles soluciones](https://github.com/platformio/platformio-core-installer/issues/221)
- Me salen errores relacionados con Python.
Es probable que tengas que instalar una versión superior de Python
  1. Instala la última versión de Python que haya aquí: https://www.python.org/downloads/ en este caso vamos a instalar 3.9.
<img src="/images/VScode/python_download.jpg" alt="download latest python" />
<img src="/images/VScode/python_download_version_ES.jpg" alt="download latest python" class="sombra" />

  2. Asegúrate de que queda registrado en el PATH del sistema.
<img src="/images/VScode/python_download_path_ES.jpg" alt="Anadir python al PATH" class="sombra" />

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>

