---
title: Lasercat
author: mifulapirus
banner:
  url: /images/laserCat/LaserCat_banner.jpg
tags: [Electronics, ESP8266, Wemos D1, Home Automation, Impresion 3D]
categories:
  - diy
date: 2021-02-14 00:00:00
---
¿Quieres que tu gato esté activo y contento? Pon un láser en su vida aunque tu no estés en casa.

En este tutorial os enseño como fabricar esta torre láser tan molona y que la podáis controlar por MQTT desde vuestro sistema de control domótico, que tanto le gusta a mi compañero de blog, o por OSC (Open Sound Control) sobre el que escribiré más adelante, pero que os permite controlarlo desde una mesa de sonido como un profesional del entretenimiento felino.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5yczNDS4Oxw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Materiales Necesarios
---------------------
Esto es lo que necesitas:
**1. [Wemos D1](https://amzn.to/3cJPUAq) x 1:** Mi tarjeta ESP preferida. Barato y potente.
**2. [Puntero Láser](https://amzn.to/3ahUXXh) x 1:** Un simple puntero láser fácil de conectar y controlar. En este paquete vienen varios, pero es el más barato y seguro que los puedes usar para otros proyectos.
**3. [Transistor](https://amzn.to/3ae4vlY) x 1:** No deberías encender el láser directamente desde el uC, así que vamos a necesitar este transistorcillo como interruptor.
**4. [PCB](https://amzn.to/3b1nH5v) x 1:** En caso de que no tengas una placa sobre la que soldar, estas son las que suelo usar yo.
**5. [Botón](https://amzn.to/3aW57M0) x 1:** ¡Le das al botón y empieza la juerga!
**6. [Servos](https://amzn.to/2N7jVQ5) x 2:** Nada del otro mundo, sólo un par de servos de los pequeños para mover el láser.
**7. Impresora 3D (o un amigo que tenga una):** A tu gato le va a dar igual si te ha quedado bonito o no, así que supongo que puedes pegarlo todo o ponerlo en plastilina, pero asó no vas a poder presumir con tus amigos.

Paso a paso
-----------
¡Vamos al lío!
**1. Electrónica:** El circuito es bastante sencillo, así que esta es tu oportunidad para hacerlo bien y que además te quede bonito.
![Esquemático](/images/laserCat/LaserCat-schematic.png)
Así es como queda en montado sobre uno de los [PCBs](https://amzn.to/3b1nH5v) de antes si usas conectores Molex, pero también puedes conectar los cables directamente.
![Electrónica](/images/laserCat/Electronics_low.jpg)
**2. Carcasa:** Vas a necesitar una impresora 3D o quizá un amigo que te eche una mano... pero bueno, si quieres montar un _LaserCat_ y necesitas ayuda, escríbenos y vemos si es posible que te lo imprimamos nosotros, que estamos empezando y aquí trabajamos por el bien de nuestra compañía felina.
Es una carcasa bastante sencilla, pero en caso de que quieras modificarla, puedes hacerlo [desde OnShape](https://cad.onshape.com/documents/8b6244add377f47cb4033dba/w/bf3f249871266d262724b973/e/3718effdfc0ad8a3cc81b2da)
![Carcasa](/images/laserCat/case.jpg)
**3. Firmware:** El código del ESP está aun por arreglar y comentar, ¡pero funciona! Échale un ojo al [repo en Github](https://github.com/Mifulapirus/LaserCat)
Puedes controlar tu _LaserCat_ de tres formas: OSC, MQTT y el botón. Yo uso las dos, pero si no lo necesitas, puedes modificar el código y adecuarlo a tus necesidades.
Asegúrate de adaptar el código en el archivo [config.json](https://github.com/Mifulapirus/LaserCat/blob/master/firmware/data/config.json.example). Casi todos mis proyectos incluyen este archivo, así que acabará siendo una coletilla en nuestros posts.
Asegúrate de que pones las IPs correctas de tu servidor de OSC

    "server_ip":"192.168.0.42",
    "port_out":9000,
    "port_in":8000,

Y del servidor de MQTT

    "mqtt_server":"192.168.0.10",

**4. Control desde Home Assistant por MQTT** 
¿Usas Home Assistant? Lo puedes controlar desde ahí.
_LaserCat_ se conecta al servidor de MQTT que hayas puesto en el archivo [config.json](https://github.com/Mifulapirus/LaserCat/blob/master/firmware/data/config.json.example)
![home assistant](/images/laserCat/ha.jpg)
Seguramente ya te suene e incluso utilices [Home Assistant](https://www.home-assistant.io/) o algún sistema de control domótico que use MQTT. En caso de que uses Home Assistant, lo único que tienes que hacer es instalarte el [Addon de Mosquitto](https://github.com/home-assistant/hassio-addons/blob/master/mosquitto/DOCS.md) y añadir un nuevo interruptor a tu _configuration.yaml_

    switch:
      - platform: mqtt
        name: "LaserCat"
        command_topic: "sq"
        payload_off: "0"
        payload_on: "2"<br>

Con esto ya te debería aparecer la nueva integración y podrás distraer a tu gato igual que puedes encender tus luces.


**5. Control por OSC** 
¿Tienes una mesa de sonido o usas [Open Stage Control](https://openstagecontrol.ammd.net/)? esta es tu oportunidad de controlar a tu gato con esos Faders.
Es posible que no te suene mucho este protocolo si no haces proyectos relacionados con sonido, teatro o conciertos, pero sé que te va a encantar y te va a enganchar como me ha enganchado a mi.
![OSC](/images/laserCat/osc.png)
En pocas palabras, [OSC (Open Sound Control)](https://en.wikipedia.org/wiki/Open_Sound_Control) es un protocolo de comunicaciones que se usa en sintetizadores, mesas de mezcla, controladores multimedia, cámaras, altavoces o cualquier cosa que pueda estar en un escenario.
Te recomiendo que le eches un ojo al mejor software de OSC que he encontrado hasta ahora que, además, es gratis y open source. También se llama [OSC](https://openstagecontrol.ammd.net/), aunque este significa [Open Stage Control](https://openstagecontrol.ammd.net/)
[Aquí tienes una plantilla para controlar tu _LaserCat_ con un Joystick desde OSC.](https://github.com/Mifulapirus/LaserCat/blob/master/OSC/laserCat-osc.json)

La interfaz tiene un "led" que indica el estado de la conexión. Para eso te hace falta usar un script que haga un echo de algunos mensajes de tu _LaserCat_. [Lo puedes encontrar aquí.](https://github.com/Mifulapirus/LaserCat/blob/master/OSC/laserCat-keepalive-responder.js)


_"Pues ya estaría"_ Espero que vuestros gatos estén tan entretenidos como lo he estado yo construyendo el _LaserCat_ :)

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>