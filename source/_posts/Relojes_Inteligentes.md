---
title: Relojes inteligentes, colaboración con MALDITA.ES
author: mifulapirus
date: 2021-03-08 20:30:00
banner:
  url: /images/relojes_inteligentes/ez430-chronos.jpg
  width: 1280
  height: 720
categories:
  - colaboraciones
tags:
  - diy
  - diwo
  - Maldita.es
  - Charla
---
¿Te suena la imagen de arriba? _Mis dieses_
Lo más normal es que no te suene de nada puesto que es un cacharro de hace más de 10 años y que no tuvo todo el impacto que podría haber tenido. 
Arduino era un jovenzuelo de 5 años cuando Texas Instruments sacó este _"reloj programable"_ con varios sensores y una pantalla LCD muy básica... básica incluso para la época, pero era un microcontrolador con pantalla que podías llevar en la muñeca... *¡por sólo 25$!... ¡¡¡en el 2010!!!*
Para mi, y para muchos frikis de la época, esta fue nuestra introducción a los _wearables_ y yo lo aproveché bastante. Aquí tenéis a un Ángel con muchas menos canas controlando un coche teledirigido con este reloj y una _Skypic_ (pero eso es una historia para otro día).
<iframe width="560" height="315" src="https://www.youtube.com/embed/DfffM5-XYOs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

¿Y por qué os cuento todo esto? Pues porque hace un par de meses colaboré en un artículo sobre _relojes inteligentes_ con [Maldita.es](https://maldita.es/), un medio de comunicación sin ánimo de lucro que me gusta leer por su neutralidad y por el buen contraste de noticias e información que hacen siempre, así que siempre es un placer poder colaborar con ellos.
En este caso, he echado una mano en un artículo titulado _[Relojes y pulseras inteligentes: hasta qué punto son fiables los datos que recogen de nuestro cuerpo](https://maldita.es/malditatecnologia/20210108/relojes-pulseras-inteligentes-fiables-datos-cuerpo/)_. 
Os animo a que le echéis un ojo si tenéis curiosidad por entender como funcionan algunos de los sensores que llevan vuestros relojes.

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>