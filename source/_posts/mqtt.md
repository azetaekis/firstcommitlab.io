---

title: MQTT, el protocolo estándar para mensajería IoT
author: yayitazale
date: 2021-03-14 14:30:00
banner:
  url: /images/mqtt/banner.png
  width: 1280
  height: 720
categories:
  - selfhosted
tags:
  - selfhosted
  - home assistant
  - guia
  - domotica
  - IoT
  - mqtt

---
Tras arrancar con nuestro core domótico [Home Assistant](https://firstcommit.dev/2021/03/11/Hass/), vamos a dejar todo listo para empezar a agregar __dispositivos IoT__ a nuestra casa virtual integrando el [__sistema de comunicación MQTT__](https://mqtt.org/).

Este sistema es un protocolo [__Publish & Subscribe__](https://en.wikipedia.org/wiki/MQTT) en el que tendremos un servicio centralizado que gestionará todos los envíos de información como las señales de los sensores y las actuaciones a las luces.

El protocolo MQTT es cada vez más utilizado tanto en el ámbito [__IoT__](https://es.wikipedia.org/wiki/Internet_de_las_cosas) como el [__IIoT__](https://en.wikipedia.org/wiki/Industrial_internet_of_things) (siendo un [estándar ISO/IEC 20922](https://www.iso.org/standard/69466.html) de mensajería Pub/Sub ligera), aunque existen otros protocolos similares como [Redis](https://redis.io/) (sirve también como buffer de datos) o [ZeroMQ](https://zeromq.org/) (es un Brokerless).

Si quieres navegar de forma rápida, este es el TOC:

1. [Conceptos básicos](#Conceptos-basicos)
2. [Instalación](#Instalacion)
3. [Configuración](#Configuracion)
4. [Integración](#Integracion)

---

# Conceptos básicos

Vamos a utilizar esta imagen como base para entender los conceptos del protocolo MQTT:

![broker-client](/images/mqtt/mqtt_1.png)
### Broker

Es el servicio centralizado al que se conectarán todos nuestros sensores y actuadores. Cada uno de ellos enviará la información con una codificación (__topic__) y de esta manera podremos utilizar el dato (__payload__) para diferentes tareas.

### Topic

Es la etiqueta que definirá a quién corresponde la información. Se construye de la siguiente manera (es un ejemplo para entenderlo de forma fácil):

  > /Mundo/País/CCAA/Provincia/Ciudad/Barrio/Casa

Con esta estructura de ejemplo, si queremos encender todas las luces de un barrio en concreto enviaremos el comando de encender las luces con el topic:

  > /Mundo/Pais/CCAA/Provincia/Ciudad/Barrio

Al llegar ese mensaje al broker, todas las bombillas que estén suscritas a ese barrio se encenderán. Si solo queremos encender una casa en concreto enviamos con el topic:

  >/Mundo/País/CCAA/Provincia/Ciudad/Barrio/Casa

En nuestra casa virtual podríamos aplicar como base el topic __*/hass/casa/habitación*__ para poder definir las diferentes estancias de nuestra casa.

### Payload

Es donde realmente reside el dato a enviar. Un sensor de temperatura enviará la información como:

  > { “Temperature”: “22.0” }

Un sensor de presencia enviará lo siguiente cuando haya detectado movimiento:

  > { “occupancy”: “true” }

Y cuando deje de detectar movimiento:

  > { “occupancy”: “off” }

Si desde hass queremos encender la luz de la oficina con el brillo al 90% enviaremos el siguiente topic y payload:

  > /hass/casa/luz_oficina/set
  > { "brightness":”90” ,"state":"ON" }

Estos mensajes no vamos a tener que escribirlos nosotros manualmente, por lo que no es necesario que profundicemos mucho más, pero es importante que entendamos el concepto general para que seamos capaces de hacer debugging en caso de que haya cualquier problema.

### Pub & Sub

Como ves, cada componente IoT responderá ante ciertos mensajes. Esta respuesta vendrá dada según suscribimos a cada componente a unos topic en concreto. En el ejemplo anterior, para automatizar un encendido automático de una luz en la oficina, suscribiremos a HASS al siguiente topic:

  > /hass/casa/sensor_oficina

Y a su vez suscribiremos a la bombilla al siguiente topic:

  > /hass/casa/luz_oficina/set

En Hass crearemos una automatización para que cuando el sensor de presencia publique el siguiente payload en el primer topic:

  > { “occupancy”: “true” }

Hass publique a su vez el siguiente payload en el segundo topic:

  > { “State”: “ON” }

### JSON

Como ves todos estos payload los he escrito en un formato que a priori es posible que no conozcas. Este formato es el [JavaScript Object Notation o JSON](https://es.wikipedia.org/wiki/JSON), un formato estándar para enviar/compartir información entre sistemas informáticos de forma ligera y entendible por humanos que viene a sustituir el ya archiconocido [XML](https://es.wikipedia.org/wiki/Extensible_Markup_Language).

Tiene una sintaxis específica pero muy versátil que nos permite construir mensajes de la complejidad necesaria. Es el mismo formato que se utiliza por ejemplo en las [API REST](https://restfulapi.net/)(un protocolo muy utilizado en mundo de servicios web).

---

# Instalación

Para instalar un broker MQTT en [nuestro Unraid](https://firstcommit.dev/2021/01/31/Unraid/), tendremos que ir como siempre a la pestaña APPS y buscar por MQTT. 

![broker-client](/images/mqtt/install_1.png)

Como vemos, hay un broker mantenido por un usuario de la comunidad que nos facilita muchísimo el trabajo. Concretamente es una implementación de [Mosquitto MQTT](https://mosquitto.org/) sobre una base de [Alpine linux](https://www.alpinelinux.org/). 

Vamos a instalarlo tal y como nos carga la plantilla, sin cambiar nada:

![plantilla](/images/mqtt/install_2.png)

Le damos __*Apply*__ y esperamos a que termine la instalación.

![done](/images/mqtt/install_3.png)

En este momento ya tenemos el Broker MQTT funcionando en nuestro servidor. Como puedes ver, el servicio tiene dos puertos configurados, por un lado el __1883__ y por otro el __9001__, más adelante veremos para qué sirven.

---

# Configuración

Por ahora lo que vamos a hacer es abrir nuestro explorador de archivos y navegaremos a la ruta del share donde se han creado los archivos de configuración del servicio, que será algo como //IP_O_NOMBRE_DE_RED_DE_TU_UNRAID/appdata/mqtt:

![share](/images/mqtt/config_1.png)

Lo que vamos a hacer es abrir el archivo __*mosquitto.conf.example*__ con nuestro editor de código.

En este archivo tenemos tres bloques:

![config_file](/images/mqtt/config_2.png)

 - Configuración general: En principio no tocamos nada. Lo único que podriamos necesitar tocar son las tres líneas sobre persistencia del dato. En principio por la naturaleza de un sistema Pub/Sub como MQTT los clientes no podrán conocer el historial de los eventos del pasado, toda la comunicación será en tiempo real. La persistencia del dato podría servir para algunos casos concretos en un futuro pero por ahora lo vamos a dejar desactivado. 

 - Configuración de RED: Como hemos visto, la conexión MQTT puede realizarse por dos puertos. El puerto 1883 sería para realizar conexiones __*inseguras*__ sin ningún tipo de cifrado y el 9001 para __conexiones seguras con cifrado TLS__. En caso de que fuéramos a tener un cliente MQTT conectado a nuestro broker desde internet, sería conveniente utilizar la conexión segura con cifrado TLS para que __nadie pueda descifrar la información que enviamos__. Nosotros por ahora solo vamos a realizar las conexiones por el puerto 1883 ya que nuestro HASS y el broker MQTT estarán __en la misma red local__ sin necesidad de salir a internet. 

 - Seguridad de user/pass: Además de esta seguridad de cifrado de los paquetes, el propio broker tiene un sistema de seguridad basado en __autenticación con usuario y contraseña__. Aquí lo importante es __desactivar__ la opción de conexión anónima ya que sería una __brecha de seguridad__.


Guardamos el archivo con el nuevo nombre __*mosquitto.conf*__.

Para terminar, vamos a crear un juego de credenciales para que en el siguiente paso podamos integrar el MQTT en HASS. Es conveniente que aprendamos bien cómo crear un usuario y contraseña ya que cada vez que conectemos un cliente MQTT al broker es conveniente crear un nuevo juego de autenticación.

Si abrimos el archivo __*passwords.README*__ nos explica los pasos a dar:

  1. Crear un archivo con el nombre __*password.txt*__
    ![password.txt](/images/mqtt/config_3.png)
  2. Abrir el archivo y escribir el juego o juegos de user pass con formato __*usuario:contraseña*__. En nuestro caso por ejemplo creamos un usuario llamado __**hass**__ y una contraseña segura (apunta esta contraseña para después)
    ![user:pass](/images/mqtt/config_4.png)
  4. __Guardamos el archivo__.

Ahora vamos la pestaña docker de HASS, hacemos click en el logo del contenedor MQTT y le damos a __*Restart*__ para que el Broker consuma el archivo __*password.txt*__ que hemos creado.
![restart](/images/mqtt/config_5.png)

# Integración

Ya tenemos todo listo, solo nos queda que HASS se conecte como cliente a este broker. Por suerte para nosotros, HASS tiene un [__cliente nativo__](https://www.home-assistant.io/integrations/mqtt/) por lo que solo tendremos que hacer lo siguiente:

  1. Abrimos nuestro Hass y vamos a configuración. Aquí vamos a integraciones y seleccionamos __*+ Añadir integración*__
  ![new_integration](/images/mqtt/integration_1.png)
  2. Buscamos por __*MQTT*__
  ![mqtt](/images/mqtt/integration_2.png)
  3. Seleccionamos y rellenamos los datos con la IP de nuestro servidor, el puerto 1883 y el juego user/pass que acabamos de crear. Seleccionamos Habilitar Descubrimiento (esto veremos para que sirve en los próximos post) y aceptamos
  ![save](/images/mqtt/integration_3.png)

*Voilá*, ya tenemos HASS conectado a nuestro broker MQTT. En el [siguiente post](https://firstcommit.dev/2021/03/19/zigbee/) ya podremos empezar a agregar elementos IoT a nuestra casa virtual a través de MQTT.

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>
