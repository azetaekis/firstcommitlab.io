---

title: Home Assistant, domótica Open Source
author: yayitazale
date: 2021-03-11 21:00:00
banner:
  url: /images/hass/banner.png
  width: 1280
  height: 720
categories:
  - selfhosted
tags:
  - selfhosted
  - home assistant
  - guia
  - domotica
  - IoT

---
Para estrenarse en el maravilloso mundo de los __microservicios en contenedores__ sobre sistema operativo para servidores domésticos que [instalamos](https://firstcommit.dev/2021/01/31/Unraid/) y [configuramos](https://firstcommit.dev/2021/02/20/Unraid_config/) en los anteriores post, vamos a empezar con [Home Assistant](https://www.home-assistant.io/). 

Esta aplicación de [código libre y gratuito](https://github.com/home-assistant) es el core de cualquier __sistema domotizado__ que se precie. Es un proyecto que ya lleva muchos años en marcha y nos permitirá integrar en la misma plataforma de forma sencilla y con una visualización elegante cualquier aparato __IoT__ que queramos meter en casa, como luces, enchufes, sensores, plantas, cerraduras y [un larguísimo etc](https://www.home-assistant.io/integrations/).

La instalación del sistema es muy rápida y este post va a ser corto ya que no vamos a entrar a configurar cada aspecto del sistema. Por ahora vamos a instalar lo básico, entender cómo funciona y en los próximos post explicaré con más detalle cómo integrar distintos elementos.

Si quieres navegar de forma rápida, este es el TOC:

1. [Prerequisitos](#Prerequisitos)
2. [Instalación](#Instalacion)
3. [Configuración](#Configuracion)
4. [Primeros pasos](#Primeros-pasos)
5. [Estamos listos](#Estamos-listos)

# Prerequisitos

Como único prerrequisito inicial, si es que no lo tienes instalado ya, necesitarás un editor de código. Mi recomendación es que instales [Visual Studio Code](https://code.visualstudio.com/) ya que lo utilizaremos también para proyectos DIY, pero puedes usar el que más te guste como [Atom](https://atom.io/) si eres un modernazo o [Notepad++](https://notepad-plus-plus.org/) si ya peinas canas. Realmente solo utilizaremos el editor de código en el futuro y en casos muy puntuales para editar archivos de configuración cuando no quede más remedio, pero es conveniente que te lo vayas instalando.

# Instalación

Para instalar Home Assistant (Hass para los amigos) es tan sencillo como ir a la pestalla __*apps*__ en Unraid y buscar por __*Home Assistant Core*__:
![Buscar el contenedor](/images/hass/install_1.png)

Hacer click en el botón de instalar:
![Coger el template](/images/hass/install_2.png)

Y en la pantalla donde definimos la configuración lo dejamos tal y como está, hacemos click en __*Apply*__ y esperamos a que descargue la imagen del contenedor y lo arranque por nosotros con la configuración y parámetros indicados.
![Run contenedor](/images/hass/install_3.png)

# Configuración
Una vez que tengamos todo listo, vamos a la pestaña de __*Docker*__ en Unraid y veremos que el contenedor está en verde. Hacemos click sobre el icono de Hass y vamos al __*WebUI*__ (que será la ip se nuestro unraid y el puerto 8123).
![Webui](/images/hass/install_4.png)

El sistema nos pedirá la información en dos pasos, el primero es relativo a nosotros como administradores del sistema:
![Admin](/images/hass/config_1.png)

El segundo paso es darle un nombre a nuestra casa virtual y ubicarla en el mundo real:
![Home](/images/hass/config_2.png)

Tras completar estos pasos ya tenemos nuestro Hass listo para empezar a agregar componentes.


# Primeros pasos
Es el momento de __salsear__ y tocar todo. Siéntete libre de navegar por las distintas pantallas y menús para coger soltura. Las visualizaciones básicas que vamos a tener por defecto al cargar el sistema por primera vez son:

__Resumen__: funciona como dashboard. Aquí podremos configurar pantallas con todas nuestras luces etc.
![Dashboard](/images/hass/config_3.png)

__Mapa__: es donde veremos ubicados todos los componentes con geolocalización, como tu móvil o el propio Hass.
![Map](/images/hass/config_4.png)

__Registros__: como su nombre indica, aquí veremos todo lo que sea relevante que ocurra en nuestro sistema
![Registry](/images/hass/config_5.png)

__Historial__: de nuevo, como su nombre indica, podremos ver lo que ha ocurrido en el pasado sobre todos los componentes que tengamos instalados
![Historic](/images/hass/config_6.png)

Finalmente es interesante también que te familiarices con los menús de configuración del propio servidor, a donde accederás haciendo click en el engranaje de la izquierda-abajo al lado de tu nombre. Este menú es muy extenso y no voy a explicar la utilidad de cada apartado por que es bastante autoexplicativo. En cualquier caso, en los próximos post utilizaremos este menú para diferentes tareas.

![config1](/images/hass/config_7.png)


# Estamos listos
Y esto es todo, es posible que te haya sabido a poco pero créeme, las posibilidades que se abren son prácticamente infinitas y con un poco de paciencia podrás llegar a tener tantos elementos como tengo yo o más:

![demo1](/images/hass/demo_1.png)

![demo2](/images/hass/demo_2.png)

![demo3](/images/hass/demo_3.png)

![demo4](/images/hass/demo_4.png)

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>
