---
title: Eginzaleak, presentación de proyectos
author: yayitazale
date: 2021-02-03 12:30:00
banner:
  url: /images/eginzaleak/eginzaleak.png
  width: 1280
  height: 720
categories:
  - diy
tags:
  - selfhosted
  - diy
  - diwo
  - Impresión 3D
  - Charla
  - Presentación
  - Covid19
---

En esta ocasión os traigo un video de la charla/presentación que di junto a Lizar Azkune en las jornadas [Eginzaleak 2020](https://www.tabakalera.eus/es/eginzaleak-2020) en [Medialab Tabakalera](https://www.tabakalera.eus/es/medialab). Os invito a ver la jornada completa ya que el resto de proyectos presentados, [como suele ser habitual Medialab](https://www.tabakalera.eus/es/medialab/proyectos), son muy interesantes.

Os dejo también el enlace a las [diapositivas que utilizo en la presentación](https://docs.google.com/presentation/d/1-zC-sjmVR7mipSjOaXqPLEL7Yix8gXgjeDGq8ZajIsI/edit?usp=sharing).

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/486825499?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<p><a href="https://vimeo.com/486825499">Eginzaleak 2020 Proiektuen aurkezpena /  Presentaci&oacute;n de proyectos</a> from <a href="https://vimeo.com/tabakalera">Tabakalera</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
<p></p>

---
<a style="display:block;font-size:16px;font-weight:500;text-align:center;border-radius:8px;padding:5px;background:#389ce9;text-decoration:none;color:#fff;" href="/Subscribe/" target="_blank"><svg style="width:10px;height:20px;vertical-align:middle;margin:0px 5px;" viewBox="0 0 21 18"><g fill="none"><path fill="#ffffff" d="M0.554,7.092 L19.117,0.078 C19.737,-0.156 20.429,0.156 20.663,0.776 C20.745,0.994 20.763,1.23 20.713,1.457 L17.513,16.059 C17.351,16.799 16.62,17.268 15.88,17.105 C15.696,17.065 15.523,16.987 15.37,16.877 L8.997,12.271 C8.614,11.994 8.527,11.458 8.805,11.074 C8.835,11.033 8.869,10.994 8.905,10.958 L15.458,4.661 C15.594,4.53 15.598,4.313 15.467,4.176 C15.354,4.059 15.174,4.037 15.036,4.125 L6.104,9.795 C5.575,10.131 4.922,10.207 4.329,10.002 L0.577,8.704 C0.13,8.55 -0.107,8.061 0.047,7.614 C0.131,7.374 0.316,7.182 0.554,7.092 Z"></path></g></svg>Suscríbete</a>
